package calculadora;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LaCalculadoraTest {

	@Test
	void testSuma() {
		int res = LaCalculadora.suma(10, 20);
		assertEquals(30,  res);
		//fail("Not yet implemented");
	}

	@Test
	void testResta() {
		int res = LaCalculadora.resta(30, 20);
		assertEquals(10,  res);
		//fail("Not yet implemented");
	}

	@Test
	void testMultiplicacio() {
		int res = LaCalculadora.multiplicacio(2, 3);
		assertEquals(5,  res);
		//fail("Not yet implemented");
		
	}
	@Test
	public void testException() {
		int res;
		try {
			res = LaCalculadora.divideix(10, 0);
			fail("Fallo: Passa per aquí si no es llança l’excepció ArithmeticException");
		}
		catch (ArithmeticException e) {
			System.out.print("La prova funciona correctament\n");
			//La prova funciona correctament
		}
             }
	@Test
	void testDivideix() {
		int res = LaCalculadora.divideix(12,  0);
		assertEquals(1,  res);
		//fail("Not yet implemented");
	}
	@Test
	void testDivisio() {
		int res = LaCalculadora.divisio(2, 2);
		assertEquals(1,  res);
		//fail("Not yet implemented");
	}
	
	

}
