package calculadora;
import java.util.Scanner;
/**
 * <h2>clase Calculadora, se utiliza calcular cosas sin saber </h2>
 * 
 * Busca información de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 2022-12
 * @author Cristina Ramírez
 * @since 23-4-2023
 */
public class LaCalculadora {
	static Scanner reader = new Scanner(System.in);
	/**
     *<h2> Main de la calculadora</h2>
    */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//comentari 2

		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                    	try {
                        resultat = divideix(op1, op2);
                    	}
                    	catch(ArithmeticException ex){
                    		System.out.print("ERROR no es pot dividir entre 0!!");
                    	}
                        if (resultat == -99)
                            System.out.print("No ha fet la divisió");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opció erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	/**
     *<h2> En cas de no introduïr els numeros que es volen calcular surtirà aquest error</h2>
    */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}
    /**
     *<h2> Surt una excepció per evitar un error al intentar dividir entre 0</h2>
    */
    public static int divideix(int num1, int num2) {
    	int res;
    	if (num2 == 0)
    		throw new java.lang.ArithmeticException("Divisió entre zero");
    	else 
    		res = num1/num2;
    		return res;
    }


    /**
     *<h2> Funció per fer el càlcul d'una suma</h2>
    */
	public static int suma(int a, int b) { /* funció */
		int res;
		res = a + b;
		return res;
	}
	/**
     *<h2> Funció per fer el càlcul d'una resta</h2>
    */
	public static int resta(int a, int b) { /* funció */
		int res;
		res = a - b;
		return res;
	}
	/**
     *<h2> Funció per fer el càlcul d'una multiplicació</h2>
    */
	public static int multiplicacio(int a, int b) { /* funció */
		int res;
		res = a * b;
		return res;
	}
	/**
     *<h2> Funció que no utilitzem per fer una divisió</h2>
    */
	public static int divisio(int a, int b) { /* funció */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opció incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}
	/**
     *<h2> Funció per saber quin càlcul es vol fer</h2>
    */
	public static char llegirCar() // funció
	{
		char car;

		System.out.print("Introdueix un carácter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	/**
     *<h2> Funció per introduïr els valors que es volen calcular</h2>
    */
	public static int llegirEnter() // funció
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	/**
     *<h2> Funció per mostrar el resultat del càlcul</h2>
    */
	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio és " + res);
	}
	/**
     *<h2> Menú de totes les operacions que es poden fer</h2>
    */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opció i ");
	}


}
